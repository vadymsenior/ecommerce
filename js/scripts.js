document.addEventListener('DOMContentLoaded', ()=>{
   console.log("DOM ready");

    /*---------MIN HEIGHT BODY---------*/
    let header = document.querySelector('header');
    let footer = document.querySelector('footer');
    let contentWrapper = document.querySelector('main');
    contentWrapper.setAttribute('style', "min-height: ".concat(window.innerHeight - footer.offsetHeight - header.offsetHeight, "px"));
    /*---------MIN HEIGHT BODY---------*/

   /*------------Menu dropdown---------*/

   let menuSwitcher = document.querySelector('#menu-switcher');

   if(menuSwitcher){
      menuSwitcher.addEventListener('click', ()=>{
         menuSwitcher.nextElementSibling.classList.toggle('show')
      })
   }

   /*------------Menu dropdown, header search---------*/

   const categoryMenuToggle = ()=>{
      let categoryMenuLists = document.querySelectorAll('.sidebar-menu-item-has-sub-menu .menu-item-arrow');
      let subMenuListWrapper = document.querySelectorAll('.sidebar-submenu-list-wrapper');
      if(categoryMenuLists){
         categoryMenuLists.forEach((item, index)=>{
            item.onclick = function(e){
               e.preventDefault();
               item.classList.toggle('active');
               subMenuListWrapper[index].classList.toggle('active');
            }
         })
      }
   }

   let sidebarMobileSwitcher = document.querySelector('.sidebar-mobile-switcher');

   if(sidebarMobileSwitcher){
      sidebarMobileSwitcher.onclick = function(){
         let sidebarNodes = document.querySelectorAll('.sidebar > *');
         for(let i =1; i<sidebarNodes.length; i++){
            sidebarNodes[i].classList.toggle('active');
         }
      }
   }

   const showHeaderSearch = ()=>{
      let searchPopup = document.querySelector('.search-popup');
      let headerSearch = document.querySelector('form.search-form');
      if(searchPopup){
         searchPopup.onclick = function(e){
            e.preventDefault();
            this.classList.toggle('active');
            headerSearch.classList.toggle('active');
         }
      }
   }

   if(window.innerWidth < 768) {
      showHeaderSearch();
   }
   if(window.innerWidth < 1025) {
      categoryMenuToggle();
   }
   window.onresize = ()=>{
      if(window.innerWidth < 768) {
         showHeaderSearch();
      }
      if(window.innerWidth < 1025) {
         categoryMenuToggle();
      }
   }


   /*-------------------SLIDERS---------------*/

   $('.homepage-top-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
   })

   $('.popular-products-slider').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: false,
      prevArrow: $('#popular-products-arr1'),
      nextArrow: $('#popular-products-arr2'),
      responsive: [
         {
            breakpoint: 1200,
            settings: {
               slidesToShow: 4,
            }
         },
         {
            breakpoint: 1024,
            settings: {
               slidesToShow: 3,
            }
         },
         {
            breakpoint: 579,
            settings: {
               slidesToShow: 2,
            }
         },
         {
            breakpoint: 476,
            settings: {
               slidesToShow: 1,
            }
         }
      ]
   })

   $('.news-products-slider').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: false,
      prevArrow: $('#news-products-arr1'),
      nextArrow: $('#news-products-arr2'),
      responsive: [
         {
            breakpoint: 1200,
            settings: {
               slidesToShow: 4,
            }
         },
         {
            breakpoint: 1024,
            settings: {
               slidesToShow: 3,
            }
         },
         {
            breakpoint: 579,
            settings: {
               slidesToShow: 2,
            }
         },
         {
            breakpoint: 476,
            settings: {
               slidesToShow: 1,
            }
         }
      ]
   })

   $('.product-page-buy-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: false,
      prevArrow: $('#popular-products-arr1'),
      nextArrow: $('#popular-products-arr2'),
      responsive: [
         {
            breakpoint: 1200,
            settings: {
               slidesToShow: 3,
            }
         },
         {
            breakpoint: 1024,
            settings: {
               slidesToShow: 2,
            }
         },
         {
            breakpoint: 767,
            settings: {
               slidesToShow: 3,
            }
         },
         {
            breakpoint: 579,
            settings: {
               slidesToShow: 2,
            }
         },
         {
            breakpoint: 476,
            settings: {
               slidesToShow: 1,
            }
         },
      ]
   })
   $('.see-with-this-prodoct-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: false,
      prevArrow: $('#news-products-arr1'),
      nextArrow: $('#news-products-arr2'),
      responsive: [
         {
            breakpoint: 1200,
            settings: {
               slidesToShow: 3,
            }
         },
         {
            breakpoint: 1024,
            settings: {
               slidesToShow: 2,
            }
         },
         {
            breakpoint: 767,
            settings: {
               slidesToShow: 3,
            }
         },
         {
            breakpoint: 579,
            settings: {
               slidesToShow: 2,
            }
         },
         {
            breakpoint: 476,
            settings: {
               slidesToShow: 1,
            }
         },
      ]
   })
   $('.product-image-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: false,
      fade: true,
      cssEase: 'linear',
      prevArrow: $('#product-image-slider-arr1'),
      nextArrow: $('#product-image-slider-arr2'),
   })

   /*-------------------SLIDERS---------------*/

   /*--------------POPUP ACTIONS--------------*/

   let popupActivates = document.querySelectorAll('.popup');
   let bodyOverlay = document.querySelector('#overlay');
   let popups = document.querySelectorAll('.popup-wrapper');
   let closePopup = document.querySelectorAll('.close-popup');

   /*---------Activate popup---------*/

   popupActivates.forEach((item)=>{
      item.onclick=(e)=>{
         e.preventDefault();
         document.body.style.overflow = 'hidden';
         bodyOverlay.classList.add('active');
         document.querySelector(`#${item.getAttribute('data-popup')}`).classList.add('active');
      }
   })

   /*---------Close popup---------*/

   let closePopupFoo = ()=>{
      popups.forEach((item)=>{
         if(item.classList.contains('active')){
            item.classList.remove('active')
         }
         document.body.style.overflow = 'visible';
         bodyOverlay.classList.remove('active');
      })
   }

   bodyOverlay.addEventListener('click', closePopupFoo);
   closePopup.forEach((item)=>{item.onclick = closePopupFoo});


   /*--------------POPUP ACTIONS--------------*/

   /*--------------UI RANGE SLIDER--------------*/

   let rangeSlider = document.getElementById('range-slider'),
       filterSliderMinValue,
       filterSliderMaxValue,
       filterSliderStepValue;
   if(rangeSlider){
      let minValueText = document.getElementById('min-range-value'),
          maxValueText = document.getElementById('max-range-value');

      filterSliderMinValue = rangeSlider.getAttribute('data-min')*1;
      filterSliderMaxValue = rangeSlider.getAttribute('data-max')*1;
      filterSliderStepValue = rangeSlider.getAttribute('data-step')*1;
      minValueText.innerHTML = filterSliderMinValue + ' р.';
      maxValueText.innerHTML = filterSliderMaxValue + ' р.';

      $('#range-slider').slider({
         range: true,
         step: filterSliderStepValue,
         min: filterSliderMinValue,
         max: filterSliderMaxValue,
         values: [filterSliderMinValue, filterSliderMaxValue],
         slide: function(event, ui){
            let minVal = ui.values[0],
                maxVal = ui.values[1];
            minValueText.innerHTML = minVal + ' р.';
            maxValueText.innerHTML = maxVal + ' р.';
         }
      })
   }

   /*--------------UI RANGE SLIDER--------------*/

   /*--------------PLAY VIDEO--------------*/

   let video = document.querySelector('#video');
   let videoTrigger = document.querySelector('#video-trigger');
    if(video && videoTrigger){
       videoTrigger.onclick = function(){
          this.parentNode.classList.add('hide');
          this.classList.add('hide');
          video.play();
       };
    }

   /*--------------PLAY VIDEO--------------*/

   /*--------------FANCYBOX--------------*/

   $('[data-fancybox="gallery"]').fancybox({
      toolbar  : false,
      loop: false,
      infobar: false
   });

   /*--------------FANCYBOX--------------*/

   /*------------------TABS------------------*/

   let tabsWrapper = document.querySelectorAll('.product-card-tabs-content-wrapper');
   let tabSwitcher = document.querySelectorAll('.tabs-switcher-item');
   let tabContent = document.querySelectorAll('.single-product-tab');

    if(tabsWrapper.length > 0){
       tabSwitcher.forEach((elem, index, arr)=>{

          elem.addEventListener('click', (e)=>{
             if(!elem.classList.contains('active')){
                document.querySelector('.tabs-switcher-item.active').classList.remove('active');
                elem.classList.add('active');
                tabContent.forEach((el)=>{
                   el.classList.remove('active');
                })
                tabContent[index].classList.add('active');
             } else {
                e.preventDefault();
             }
          })
       })
    }

   /*------------------TABS------------------*/

});
